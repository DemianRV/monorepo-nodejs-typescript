FROM keymetrics/pm2:10-alpine
# Compile base image from source. We need to remove volume command in base image
LABEL maintainer="dadmian@gmail.com"

# Configure NPM
#       Moved to save layers modifications
ENV NPM_CONFIG_LOGLEVEL warn

# Only need install if we change package.json
COPY ./package*.json ./

RUN npm ci --prod

# Code can be transpile allways
COPY ./pm2.json ./pm2.json

# TODO: cambiar por el nombre de la app
COPY ./dist/apps/api ./build

CMD [ "pm2-docker", "start", "pm2.json" ]
