export interface Pelicula {
  _id: string;
  nombre: string;
  descripcion: string;
  duracion?: number;
  directores?: [string];
  actores?: [string];
}

export interface PeliculaToCreate {
  nombre: string;
  descripcion: string;
  duracion?: number;
  directores?: [string];
  actores?: [string];
}

export interface DatosPeliculaParaActualizar {
  nombre?: string;
  descripcion?: string;
  duracion?: number;
  directores?: [string];
  actores?: [string];
}