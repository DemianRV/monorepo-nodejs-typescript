import * as mongoose from 'mongoose';

mongoose.connect('mongodb://localhost:27017/mi-primera-bbdd')
        .then( () => {
          console.log('Conexion con la bbdd realizada')
        })
        .catch( err => {
          console.error(err)
        });
