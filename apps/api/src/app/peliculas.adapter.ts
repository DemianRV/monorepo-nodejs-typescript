import { PeliculaModel } from './peliculas.model';
import {
  Pelicula,
  PeliculaToCreate,
  DatosPeliculaParaActualizar,
} from './pelicula.interface';

export async function crear_pelicula(pelicula_to_create: PeliculaToCreate): Promise<Pelicula> {
  try {

    const pelicula_creada = await PeliculaModel.create( pelicula_to_create );
    return pelicula_creada.toObject();

  } catch (error) {
    return Promise.reject( error )
  }
}

export async function obtener_todas_las_peliculas(): Promise<Pelicula[]> {
  try {

    const peliculas = await PeliculaModel.find();
    return peliculas.map( pelicula => pelicula.toObject() )

  } catch (error) {
    return Promise.reject( error )
  }
}

export async function obtener_una_pelicula_dado_un_nombre(nombre: string): Promise<Pelicula> {
  try {

    const pelicula_encontrada = await PeliculaModel.findOne({ nombre });

    if ( !pelicula_encontrada ) {
      return Promise.reject('Pelicula no encontrada');
    }

    return pelicula_encontrada.toObject();

  } catch (error) {
    return Promise.reject( error )
  }
}

export async function actualizar_pelicula( nombre: string, datos_pelicula_para_actualizar: DatosPeliculaParaActualizar ) {
  try {

    const pelicula_actualizada = await PeliculaModel.findOneAndUpdate({ nombre: nombre }, datos_pelicula_para_actualizar)
    return pelicula_actualizada.toObject();

  } catch (error) {
    return Promise.reject( error )
  }
}

export async function eliminar_pelicula( nombre: string) {
  try {

    const pelicula_actualizada = await PeliculaModel.findOneAndRemove({ nombre: nombre })

    if (!pelicula_actualizada) {
      return Promise.reject('Pelicula no encontrada');
    }

    return pelicula_actualizada.toObject();

  } catch (error) {
    return Promise.reject( error )
  }
}
