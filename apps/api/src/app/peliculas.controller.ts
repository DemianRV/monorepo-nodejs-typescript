import { Request, Response, NextFunction } from 'express';
import * as PeliculasAdapter from './peliculas.adapter';

export async function crear_pelicula(req: Request, res: Response, next: NextFunction) {

  const pelicula_to_create = req.body.pelicula_to_create;

  if (!pelicula_to_create) {
    return next('pelicula_to_create es requerido')
  }

  try {

    const pelicula_creada = await PeliculasAdapter.crear_pelicula(pelicula_to_create)
    return res.send({ pelicula_creada });

  } catch (error) {
    return next(error)
  }
}

export async function obtener_todas_las_peliculas(req: Request, res: Response, next: NextFunction) {

  try {

    const peliculas = await PeliculasAdapter.obtener_todas_las_peliculas()
    return res.send({ peliculas });

  } catch (error) {
    return next(error)
  }
}

export async function obtener_una_pelicula_dado_un_nombre(req: Request, res: Response, next: NextFunction) {

  const nombre_de_la_pelicula_que_queremos = req.body.nombre;

  if (!nombre_de_la_pelicula_que_queremos) {
    return next('nombre es requerido')
  }

  try {

    const pelicula_encontrada = await PeliculasAdapter.obtener_una_pelicula_dado_un_nombre(nombre_de_la_pelicula_que_queremos)
    return res.send({ pelicula_encontrada });

  } catch (error) {
    return next(error)
  }
}

export async function actualizar_pelicula(req: Request, res: Response, next: NextFunction) {
  const nombre_de_la_pelicula_que_queremos_actualizar = req.body.nombre;
  const nuevos_datos = req.body.nuevos_datos;

  if (!nombre_de_la_pelicula_que_queremos_actualizar) {
    return next('nombre es requerido')
  }

  if (!nuevos_datos) {
    return next('nuevos_datos es requerido')
  }

  try {

    const pelicula_encontrada = await PeliculasAdapter.actualizar_pelicula(nombre_de_la_pelicula_que_queremos_actualizar, nuevos_datos)
    return res.send({ pelicula_encontrada });

  } catch (error) {
    return next(error)
  }
}

export async function eliminar_pelicula(req: Request, res: Response, next: NextFunction) {
  const nombre_de_la_pelicula_que_queremos_eliminar = req.body.nombre;

  if (!nombre_de_la_pelicula_que_queremos_eliminar) {
    return next('nombre es requerido')
  }

  try {

    const pelicula_encontrada = await PeliculasAdapter.eliminar_pelicula(nombre_de_la_pelicula_que_queremos_eliminar)
    return res.send({ pelicula_encontrada });

  } catch (error) {
    return next(error)
  }
}