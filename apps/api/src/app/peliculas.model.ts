import { Schema, model } from 'mongoose'

const PeliculaSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    index: true,
  },
  descripcion: {
    type: String,
    required: true,
    index: true,
  },
  duracion: Number,
  directores: [String],
  actores: [String]
})


export const PeliculaModel = model('Pelicula', PeliculaSchema)
