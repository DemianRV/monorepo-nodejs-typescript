import { Router } from 'express';
import * as PeliculasController from './peliculas.controller';

const rutas_de_pelicula = Router();

rutas_de_pelicula.post('/crear-pelicula', PeliculasController.crear_pelicula)
rutas_de_pelicula.get('/obtener-todas-las-peliculas', PeliculasController.obtener_todas_las_peliculas)
rutas_de_pelicula.post('/obtener-una-peliculas-dado-un-nombre', PeliculasController.obtener_una_pelicula_dado_un_nombre)
rutas_de_pelicula.post('/actualizar-una-pelicula-dado-un-nombre', PeliculasController.actualizar_pelicula)
rutas_de_pelicula.delete('/eliminar-una-pelicula-dado-un-nombre', PeliculasController.eliminar_pelicula)

export default rutas_de_pelicula;