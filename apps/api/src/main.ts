/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/

import * as express from 'express';
import * as bodyParser from 'body-parser';
import rutas_de_pelicula from './app/peliculas.routes';

import './app/db';
const app = express();

app.use( bodyParser.json() );

app.get('/api', (req, res) => {
  res.send({message: `Welcome to api!`});
});

app.use('/peliculas', rutas_de_pelicula)

// Ruta no encontrada
app.use((req, res, next) => {
  return res.status(404).send({ mensaje: 'Url no existe' });
});

// Error interno
app.use((error, req, res, next) => {
  console.error(error);
  return res.status(500).send({ mensaje: 'Hubo un error', traza: error });
})

const port = process.env.port || 3333;
app.listen(port, (err) => {
  if (err) {
    console.error(err);
  }
  console.log(`Listening at http://localhost:${port}`);
});
