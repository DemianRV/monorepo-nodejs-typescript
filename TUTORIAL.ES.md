# Introducción a NodeJS y Typescript

Vamos a realizar una introducción a crear aplicaciones con NodeJS y Typescript, para ello vamos a crear un proyecto que consiste en una Api Rest que a su vez de conecta una base de datos MongoDB.

Para ello a modo ilustrativo estos son los pasos a seguir durante el taller:
+ 1. Creación de la estructura del proyecto
+ 2. Inicialización del repositorio
+ 3. Creación de nuestra aplicación
+ 4. Nuestro primer commit y push
+ 5. Arrancar nuestro proyecto
+ 6. Conexión con la BBDD
+ 7. Creación de nuestro primer 'Schema'(Collección de MongoDb)
+ 8. Operaciones CRUD sobre nuestras películas
+ 9. Creación de nuestra api. (Hello, world!)
+ 10. Creación de nuestro primer 'end point'
+ 11. Creación end point ruta no encontrada (404) y error interno (500)
+ 12. Instalando body parser
+ 12. Creacion de nuestras rutas

## Paso 0: Requisitos y herramientas.

+ [NodeJS (a partir de la versión 10)](https://nodejs.org/es/)
+ [Docker](https://docs.docker.com/install/)
+ [Docker Compose](https://docs.docker.com/compose/install/)
+ [Visual Studio Code](https://code.visualstudio.com) (opcional, es el editor que usará el ponente)
+ [MongoDB](https://docs.mongodb.com/manual/administration/install-community) (opcional, si tenemos [Docker](https://docs.docker.com/install/) o tenemos cuenta en [MongoDB Atlas](https://www.mongodb.com/cloud/atlas))

Mencionar Gitlab y NX

## Paso 1: Creación de la estructura del proyecto
### Ejecutamos el siguiente comando donde monorepo-nodejs-typescript es el nombre de nuestro proyecto.

```console
npx create-nx-workspace monorepo-nodejs-typescript
```

### Acto seguido nos preguntará ciertos aspectos, para crear nuestro proyecto.

La primera de las preguntas no nos afecta para este taller, pues no usaremos hojas de estilo de ningún tipo. Esta configuración es por si mas adelante quisiéramos crear aplicaciones de Angular, React o Webcomponent.

Para este proyecto hemos contestado de la siguiente forma:

+ ? Which stylesheet format would you like to use? SCSS

SCSS

+ ? What is the npm scope you would like to use for your Nx Workspace?

Pulsamos intro (lo dejamos vacío)

+ ? What to create in the new workspace (You can create other applications and libraries at any point using 'ng g')

Seleccionamos empty y pulsamos intro

Acto seguido se creará nuestro proyecto con la configuración elegida, tambien se instalarán ciertas dependecias como puede se typescript, tslint o prettier.


## Paso 2: Inicialización del repositorio
En nuestro caso usaremos [Gitlab](https://gitlab.com) que además de un repositorio nos ofrece más herramientras como un registro propio para contenedores docker, tambien nos ofrece unsistema de integración continua, el cual veremos al fina del taller.

 + Procedemos a entrar y registrarnos [Gitlab](https://gitlab.com).
 + Creamos un nuevo proyecto (arriba derecha hay un botón verde, segun el idioma aparecerá un texto diferente), pulsando el botón de "new proyect".
 + Acto seguido añadimos un titulo a nuestro proyecto y pulsamos en crear (podeis ver que existen más opciones).
 + Una vez creado si hacemos scrolling podremos ver la opción que necesitamos para poder subir nuestro proyecto.


## Paso 3: Creación de nuestra aplicación de NodeJS
Ejecutaremos el siguiente comando, en este caso escogeremos Express como framework

```console
ng g node-application api
```


## Paso 4: Nuestro primer commit y push
Vamos a iniciar nuestro sistema de control de versiones para poder tener una trazabilidad, así como una GRAN AYUDA para desarrollar cualquier proyecto.

+ Abrimos una terminal en la carpeta de nuestro proyecto.
+ Vamos a gitlab y seguimos la instrucciones para subir nuestro proyecto.


## Paso 5: Arrancar nuestro proyecto
En la carpeta del proyecto abrimos una terminal
```console
ng serve nombre-del-proyecto
```

Ahora si accedemos desde el navegador, obtendremos respuesta, en este caso no hay nada por eso el mensaje de error.

[http://localhost:3333](http://localhost:3333)

Sin embargo, si accedemos aquí obtendremos un mensaje de bienvenida.

[http://localhost:3333/api](http://localhost:3333/api)


## Paso 6: Conexión con la BBDD

¡Es importante tener la base de datos arrancada para que podamos conectarnos! (Parece obvio pero las prisas y los nervios hacen que nos olvidemos de esto)

### Instalar Mongoose (nos ayudará a trabajar con mongodb)

Vamos a instalar nuestro primer paquete de forma manual, en este caso será [mongoose](https://mongoosejs.com/) el cual nos facilitará la interacción con la base de datos:

```console
npm install mongoose
```

Algunos paquetes no son reconocidos por typescript, y necesitan que se instalen otro paquete para que typescript 'entienda' como funciona dicho paquete.
En nuestro caso eso pasa con mongoose, asi pues, instalamos los 'types' de mongoose con el siguiente comando.

```console
npm install --save-dev @types/mongoose
```

Cabe destacar el 'flag' --save-dev con el cual indicamos que esta dependencia solo es necesaria para el desarrollo de nuestra aplicación y no para el funcionamiento de esta (uso en producción o por terceros).

### Conectandonos a MongoDB con Mongoose

Para ello crearemos un archivo (ej: db.ts ) dentro de la carpeta app en el usaremos mongoose.

¡Muy importante!, debemos importar este archivo en main.ts para que nodejs lo ejecute pues nuestro punto de entrada es el archivo main.ts


## Paso 7: Creación de nuestro primer 'Schema' y 'Model'

Vamos a crear nuestro primer Esquema ('Schema') que básicamente nos sirve para indicarle a mongoose QUE y COMO debe guardar nuestra información.
Mientras que con nuestro Modelo ('Model') le diremos DONDE.

Creamos un archivo (ej: peliculas.model.ts) y en el crearemos nuestro primer [esquema](https://mongoosejs.com/docs/guide.html#definition) y nuestro primer [modelo](https://mongoosejs.com/docs/guide.html#models)


## Paso 8: Operaciones CRUD sobre nuestras películas

Creamos un archivo (ej: peliculas.adapter.ts) el cual usaremos para hacer OPERACIONES SIMPLES sobre nuestras películas.

Crearemos 5 funcionalidades
+ Crear una pelicula
+ Obtener una pelicula dado un nombre
+ Obtener todas las películas
+ Actualizar una pelicula dado un nombre
+ Eliminar una película

## Paso 12: Instalando body parser

Body parser es una librería que usaremos para "ayudar" a express los diferentes tipos de peticiones.

```console
npm install body-parser
```

Body parser tambien necesita actualmente los "types"

```console
npm install --save-dev @types/body-parser
```